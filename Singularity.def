BootStrap: docker
From: bioconductor/bioconductor_docker:RELEASE_3_16

%labels
    Maintainer Josh Granek
    Image_Name meta_methylome_bioconductor
    Image_Version meta_methylome_bioconductor_v001a

%environment
  export SHELL=/bin/bash
  export LC_ALL=en_US.UTF-8
  export LANG=en_US.UTF-8
  export LANGUAGE=en_US.UTF-8
  export PATH=${PATH}:/opt/bin:/opt/ont/guppy/bin

%post
  export DEBIAN_FRONTEND="noninteractive"

  apt-get update

  # Install packages necessary for building dorado from source
  apt-get install -y --no-install-recommends \
    curl \
    git \
    ssh \
    ca-certificates \
    python3-pip \
    less \
    lsof \
    samtools \
    minimap2 \
    libbz2-dev \
    liblzma-dev \
    vim \
    libpthread-stubs0-dev \
    lbzip2 \
    seqkit \
    awscli \
    htop \
    bioawk \
    metabat \
    cargo \
    python-is-python3 \
    pigz

  # Install packages necessary for tidyverse
  apt-get install -y --no-install-recommends \
    libcurl4-gnutls-dev \
    libxml2-dev \
    libssl-dev \
    zlib1g-dev \
    libhdf5-dev \
    hdf5-tools

  #------------------------------------------------------------------
  # Install Guppy for Demuxing
  # https://community.nanoporetech.com/docs/prepare/library_prep_protocols/Guppy-protocol/v/gpb_2003_v1_revaw_14dec2018/linux-guppy
  #------------------------------------------------------------------
  mkdir -p /opt/ont/guppy
  curl -L -s https://cdn.oxfordnanoportal.com/software/analysis/ont-guppy-cpu_6.5.7_linux64.tar.gz | \
      tar -zx -C /opt/ont/guppy --strip-components=1
  # wget https://cdn.oxfordnanoportal.com/software/analysis/ont-guppy_6.5.7_linux64.tar.gz

  #===========================================================================
  # Following Toolchain based on https://pubmed.ncbi.nlm.nih.gov/35789207/
  # https://github.com/Serka-M/Digester-MultiSequencing
  # See mmlong2-lite for snakemake version https://github.com/Serka-M/mmlong2-lite
  #===========================================================================
  #------------------------------------------------------------------
  # Install Flye
  # https://github.com/fenderglass/Flye
  #------------------------------------------------------------------
  mkdir Flye && \
    curl -L -s https://github.com/fenderglass/Flye/archive/refs/tags/2.9.2.tar.gz | \
      tar -zx -C Flye --strip-components=1 && \
    cd Flye
    python setup.py install
    cd ..
    rm -rf Flye

  #------------------------------------------------------------------
  # Install NanoPack
  # https://github.com/wdecoster/nanopack
  #------------------------------------------------------------------
  pip3 install --no-cache-dir nanopack

  #------------------------------------------------------------------
  # Install CoverM
  # https://github.com/wwood/CoverM
  #------------------------------------------------------------------
  cargo install coverm

  #------------------------------------------------------------------
  # Install Vamb
  # https://github.com/RasmussenLab/vamb
  #------------------------------------------------------------------
  pip3 install --no-cache-dir vamb

  #------------------------------------------------------------------
  # Install dRep
  # https://github.com/MrOlm/drep
  #------------------------------------------------------------------
  pip3 install --no-cache-dir drep

  #------------------------------------------------------------------
  # Install MaxBin
  # https://sourceforge.net/p/maxbin/code/ci/master/tree/
  #------------------------------------------------------------------
  echo "Need to install MaxBin"

  #------------------------------------------------------------------
  # Install DAS Tool
  # https://github.com/cmks/DAS_Tool
  #------------------------------------------------------------------
  echo "Need to install DAS Tool"

  #------------------------------------------------------------------
  # Install Dorado
  # https://github.com/nanoporetech/dorado/
  #------------------------------------------------------------------
  mkdir -p /opt/bin /opt/lib /opt/hdf5
  mkdir dorado && \
    curl -L -s https://cdn.oxfordnanoportal.com/software/analysis/dorado-0.3.4-linux-x64.tar.gz | \
      tar -zx -C dorado --strip-components=1 && \
    mv dorado/bin/* /opt/bin && \
    mv dorado/lib/* /opt/lib && \
    mv dorado/hdf5/* /opt/hdf5 && \
    rm -rf dorado

  #------------------------------------------------------------------
  # Install POD5 Utilities
  # https://github.com/nanoporetech/pod5-file-format/issues
  #------------------------------------------------------------------
  pip3 install --no-cache-dir pod5

  #------------------------------------------------------------------
  # Install Medaka
  # https://github.com/nanoporetech/medaka
  #------------------------------------------------------------------
  pip3 install --no-cache-dir medaka

  #------------------------------------------------------------------
  # Install Modkit
  # https://github.com/nanoporetech/modkit
  #------------------------------------------------------------------
  mkdir -p /opt/bin
  mkdir modkit && \
    curl -L -s https://github.com/nanoporetech/modkit/releases/download/v0.1.9/modkit_v0.1.9_centos7_x86_64.tar.gz | \
      tar -zx -C modkit --strip-components=1 && \
    mv modkit/modkit /opt/bin
    rm -rf modkit
    
  #------------------------------------------------------------------
  # Install R Packages
  #------------------------------------------------------------------
   Rscript -e "install.packages(pkgs = c('here','markdown','tidyverse', 'R.utils'), \
     repos='https://archive.linux.duke.edu/cran/', \
     dependencies=TRUE, \
     clean = TRUE)"
     
  #------------------------------------------------------------------
  # Install R Bioconductor Packages
  #------------------------------------------------------------------
  Rscript -e "BiocManager::install(c(\
    'Rhtslib',
    'Rsamtools',
    'bamsignals',
    'GenomicAlignments',
    'GenomicRanges',
    'ggbio',
    'biovizBase'), 
    clean = TRUE)"

  #===============================
  # DO NOT PUT ANYTHING BELOW HERE
  #===============================
  #------------------------------------------------------------------
  # Clean up!
  #------------------------------------------------------------------
  apt-get clean && \
    rm -rf /var/lib/apt/lists/*    
